'use strict';

const {
    username,
    password,
    vhost,
    heartbeat,
    exchange,
    queue,
    endpointsOptions
} = require('./config.bus');

const amqplib = require('amqplib');
const endpoints = require('endpoint-registry').resolve(endpointsOptions);

let connectionOptions = endpoints.next();
let amqpChannel = null;
const messages = [];
let messageHandler = null;

endpoints.on('change', ({ address, port }) => {
    connectionOptions = Promise.resolve({ address, port });
    amqpChannel = null;
});

async function consume(channel) {
    channel[Symbol.asyncIterator] = async function* () {
        while (true) {
            yield messages.length > 0
                ? Promise.resolve(messages.shift())
                : new Promise(resolve => messageHandler = resolve)
        }
    };
    channel.consume(queue, message => {
        messages.push(message.content.toString());
        if (messageHandler) {
            messageHandler(messages.shift());
            messageHandler = null;
        }
    }, { noAck: true });
}

async function channel() {
    if (null === amqpChannel) {
        amqpChannel = new Promise(async resolve => {
            const { address, port } = await connectionOptions;
            const con = await amqplib.connect(`amqp://${username}:${password}@${address}:${port}/${vhost}/?heartbeat=${heartbeat}`);
            const channel = await con.createChannel();
            await channel.assertExchange(exchange, 'fanout');
            await channel.assertQueue(queue, { durable: true, autoDelete: false });
            await channel.bindQueue(queue, exchange);
            consume(channel);
            resolve(channel);
        });
    }

    return amqpChannel;
}

async function* get() {
    for await (let message of await channel()) {
        yield message;
    }
}

async function send(message) {
    await (await channel()).publish('Test', '', Buffer.from(JSON.stringify(message)));
}

module.exports = { get, send };
