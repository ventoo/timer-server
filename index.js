'use strict';

require('console-stamp')(console, { pattern: 'dd/mm/yyyy HH:MM:ss.l' });

const bus = require('./bus');
const timer = require('./timer');

(async () => {
    timer.process(1, message => {
        bus.send(message);
    });

    for await (let message of bus.get()) {
        timer.schedule({ body: message }, 1000);
    }
})();
