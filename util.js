'use strict';

const crypto = require('crypto');

const uuid = () => crypto.randomBytes(16).toString('hex');

module.exports = {
    uuid
};
