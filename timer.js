'use strict';

const {
    queue,
    endpointsOptions
} = require('./config.timer');

const Bull = require('bull');
const endpoints = require('endpoint-registry').resolve(endpointsOptions);

let connectionOptions = endpoints.next();
let bullQueue = null;

endpoints.on('change', ({ address, port }) => {
    connectionOptions = Promise.resolve({ address, port });
    bullQueue = null;
});

const redis = {
    enableReadyCheck: true,
    maxRetriesPerRequest: null,
    connectTimeout: 60000
};

const settings = {
    lockDuration: 30000,
    stalledInterval: 5000,
    maxStalledCount: 2,
    guardInterval: 5000,
    retryProcessDelay: 5000,
    drainDelay: 5
};

const limiter = {
    max: 10000,
    duration: 1000,
    bounceBack: false
};

const defaultJobOptions = {
    removeOnComplete: true,
    removeOnFail: false
};

async function bull() {
    if (null === bullQueue) {
        bullQueue = new Promise(async resolve => {
            const { address, port } = await connectionOptions;
            resolve(new Bull(queue, { redis: { host: address, port, ...redis }, settings, limiter, defaultJobOptions }));
        });
    }

    return bullQueue;
}

async function process(concurrency, callback) {
    (await bull()).process(concurrency, (job, done) => {
        callback(job.data);
        done();
    });
}

async function schedule(message, delay) {
    await (await bull()).add(message, { delay });
}

module.exports = { process, schedule };
